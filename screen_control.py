from sense_hat import SenseHat
import time
import random

sense = SenseHat()

# messages and jokes, messages at the end
messages = ["How do you send a computer into space? You click the space bar!", "What is a spaceman's favorite chocolate? A Marsbar!", "What's a light year? The same as a regular year but with less calories!",
            "What do planets like to read? Comet Books!", "Why was the cow an astronaut? To visit the milky way!", "Where does a astronaut park his car? Next to the parking meteor!",
            "What did the libarian say to the astronaut? Find space for a book!", "What sweets do aliens like? Flying Saucers!", "How do you organise a outer space party? You planet."]

pixels = [
    [255, 0, 0], [255, 0, 0], [255, 87, 0], [255, 196, 0], [205, 255, 0], [95, 255, 0], [0, 255, 13], [0, 255, 122],
    [255, 0, 0], [255, 96, 0], [255, 205, 0], [196, 255, 0], [87, 255, 0], [0, 255, 22], [0, 255, 131], [0, 255, 240],
    [255, 105, 0], [255, 214, 0], [187, 255, 0], [78, 255, 0], [0, 255, 30], [0, 255, 140], [0, 255, 248], [0, 152, 255],
    [255, 223, 0], [178, 255, 0], [70, 255, 0], [0, 255, 40], [0, 255, 148], [0, 253, 255], [0, 144, 255], [0, 34, 255],
    [170, 255, 0], [61, 255, 0], [0, 255, 48], [0, 255, 157], [0, 243, 255], [0, 134, 255], [0, 26, 255], [83, 0, 255],
    [52, 255, 0], [0, 255, 57], [0, 255, 166], [0, 235, 255], [0, 126, 255], [0, 17, 255], [92, 0, 255], [201, 0, 255],
    [0, 255, 66], [0, 255, 174], [0, 226, 255], [0, 117, 255], [0, 8, 255], [100, 0, 255], [210, 0, 255], [255, 0, 192],
    [0, 255, 183], [0, 217, 255], [0, 109, 255], [0, 0, 255], [110, 0, 255], [218, 0, 255], [255, 0, 183], [255, 0, 74]
]

def run():
	while True:
		sense.set_pixels(pixels)
		time.sleep(5)
		sense.show_message(random.choice(messages))
		time.sleep(5)
		

