import numpy as np
import matplotlib.pyplot as plt

def relu(x, deriv=False):
    if deriv == True:
        x[x<=0] = 0
        x[x>0] = 1
        return x
    return x * (x > 0)

class Network:
    def __init__(self):
        np.random.seed(1)
        self.syn0 = 2 * np.random.random((4, 1)) - 1
    def show_relu(self):
        x_axis = np.arange(-5, 5, 0.2)
        plt.plot(x_axis, relu(x_axis))
        plt.show()
    def train(self, l0, correct_answer):
        print("l0: ", l0)
        print("synapses: ", self.syn0)
        l1 = relu(np.dot(l0, self.syn0))
        print("l1: ", l1)
        l1_error = correct_answer - l1
        print("l1_error: ", l1_error)
        l1_delta = l1_error * relu(l1, True)
        print("l1_delta:", l1_delta)
        print(np.dot(l0.T, l1_delta))
        self.syn0 += np.dot(l0.T, l1_delta)
    def predict(self, l0):
        return relu(np.dot(l0, self.syn0))

net = Network()
for iter in range(15000):
	net.train(np.array([[3, 7, 2, 11], [4, 5, 1, 3]]), np.array([[5], [3]]))
print(net.predict(np.array([[3, 8, 1, 9]])))
