from threading import Thread
import screen_control
import database
import functions

def run_data_program():
    db = database.Database()

Thread(target=screen_control.run()).start()
Thread(target=run_data_program).start()
Thread(target=functions.functions(functions.pixels, functions.sense)).start()

