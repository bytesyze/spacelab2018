from sklearn import linear_model
import threading
import time
import numpy as np

class Model:
    def __init__(self):
        self.model = linear_model.LinearRegression()
        self.training_data_x = None
        self.training_data_y = None
        # self.coefficients = self.model.coef_
        self.timer_thread = threading.Thread(target=self.timer)
        self.timer_thread.daemon = False
        self.timer_thread.start()
    def timer(self):
        while True:
            if self.training_data_x == None:
                time.sleep(900)
                self.train()
    def train(self):
        self.model.fit(self.training_data_x, self.training_data_y)
        self.coefficients = self.model.coef_
    def predict(self, prediction_data_x):
        return self.model.predict(prediction_data_x)
