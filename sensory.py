from sense_hat import SenseHat
import threading
import time

sense = SenseHat()

class Sensory:
    def __init__(self, timer_interval, callback):
        self.timer_interval = timer_interval
        self.callback = callback
        
        self.timer_thread = threading.Thread(callback=self.timer)
        self.timer_thread.daemon = False
        self.timer_thread.start()
    def timer(self):
        while True:
            self.temperature = sense.get_temperature()
            self.humidity = sense.get_humidity()
            self.pressure = sense.get_pressure()

            self.callback(self.temperature, self.humidity, self.pressure)
            
            time.sleep(self.timer_interval)
        
