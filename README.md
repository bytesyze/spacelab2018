Bytesyze: AstroPi SpaceLab 2017-2018
====================================

About SpaceLab
--------------
AstroPi is an annual competition run by the Raspberry Pi Foundation in partnership with the European Space Agency.
SpaceLab is a scientific mission for young people no older than 19. Design an experiment, receive free computer hardware to work with, and write the Python code to carry it out.
Your code could be uploaded to the International Space Station and run for three hours (two orbits).

About Us
--------
Dan 'Pikzel' Lewis is a game developer and makes machine learning and data science programs in Python.

- http://pikzel.itch.io
- http://pikzeldev.com

Martin Perreau-Saussine is a web developer, a Raspberry Pi enthusiast and a game developer.

Extra Notes
-----------
Temperature measurements are in degrees Celsius.
Runtime is in seconds.

To run you will need to use Python 2, and you should run main.py.
You will also need to install the Python packages listed in bitbucket-pipelines.yml

Credits
-------
SciKit Learn was used in this project
