import time
import threading

class Timer:
    def timer(self):
        while True:
            time.sleep(1)
            self.runtime += 1
    def __init__(self):
        self.runtime = 0
        timer = threading.Thread(target=self.timer)
        timer.daemon = False
        timer.start()
