from tinydb import Query, TinyDB
import time
from timer import Timer
from sensory import Sensory
from linear_regression import Model

timing = Timer()

class Database:
    def __init__(self):
        self.db = TinyDB('db.json')
        self.sensors = Sensory(1, self.insert_data)

        self.humidity_pred_model = Model()
        self.temperature_pred_model = Model()
        self.pressure_pred_model = Model()
        self.models = (self.humidity_pred_model, self.temperature_pred_model, self.pressure_pred_model)
    def insert_data(self, humidity, temperature, pressure): # I decided not to use the gyroscope or the accelerometer because they likely don't have any relationship with the other values
        self.db.insert({'humidity': humidity, 'temperature': temperature, 'pressure': pressure, 'runtime': timing.runtime})

        if self.humidity_pred_model.training_data_x == None: # If this is the first time the data is being input into the models...
            self.humidity_pred_model.training_data_x = np.array([[temperature, pressure]])
            self.humidity_pred_model.training_data_y = np.array([[humidity]])

            self.pressure_pred_model.training_data_x = np.array([[humidity, temperature]])
            self.pressure_pred_model.training_data_y = np.array([[pressure]])
            
            self.temperature_pred_model.training_data_x = np.array([[humidity, pressure]])
            self.temperature_pred_model.training_data_y = np.array([[temperature]])
        else:
            self.humidity_pred_model.training_data_x = np.append(self.humidity_pred_model.training_data_x, [[temperature, pressure]])
            self.humidity_pred_model.training_data_y = np.append(self.humidity_pred_model.training_data_y, [[humidity]])
            
            self.pressure_pred_model.training_data_x = np.append(self.pressure_pred_model.training_data_x, [[humidity, temperature]])
            self.pressure_pred_model.training_data_y = np.append(self.pressure_pred_model.training_data_y, [[pressure]])

            self.temperature_pred_model.training_data_x = np.append(self.temperature_pred_model.training_data_x, [[humidity, pressure]])
            self.temperature_pred_model.training_data_y = np.append(self.temperature_pred_model.training_data_y, [[temperature]])
